Name:          libsodium
Summary:       The Sodium crypto library
Version:       1.0.16
Release:       7
License:       ISC
URL:           http://libsodium.org/
Source0:       http://download.libsodium.org/libsodium/releases/old/libsodium-%{version}.tar.gz

BuildRequires: gcc
Obsoletes:     libsodium23 <= %{version}

%description
Sodium is a modern, easy-to-use software library for encryption, decryption, signatures, password
hashing and more. It is a portable, cross-compilable, installable6, packageable fork of NaCl, with
a compatible API, and an extended API to improve usability even further.

Its goal is to provide all of the core operations needed to build higher-level cryptographic tools.

%package       devel
Summary:       Development headers and libraries files for libsodium

Requires:      libsodium = %{version}-%{release}
Provides:      libsodium-static = %{version}-%{release}
Obsoletes:     libsodium23-devel <= %{version} libsodium-static < %{version}-%{release}

%description   devel
This package contains libraries and header files for developing applications that use libsodium libraries.

%prep
%autosetup -p1

%build
%configure --disable-silent-rules --disable-opt
%make_build

%install
%make_install

%check
make check

%files
%license LICENSE
%{_libdir}/libsodium.so.23*

%files devel
%doc AUTHORS ChangeLog README.markdown THANKS
%doc test/default/*.{c,exp,h} test/quirks/quirks.h
%{_includedir}/sodium.h
%{_includedir}/sodium/
%{_libdir}/libsodium.so
%{_libdir}/pkgconfig/libsodium.pc
%{_libdir}/libsodium.a
%exclude %{_libdir}/libsodium.la

%changelog
* Thu Jan 15 2019 Lei Zhang <ricky.z@huawei.com> - 1.0.16-7
- modify the unavailable source address

* Wed Nov 13 2019 Lijin Yang <yanglijin@huawei.com> - 1.0.16-6
- init package
